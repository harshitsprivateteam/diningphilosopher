/********************************************************************
 
 ***** dining.c *****
 
 Computer Science 6823 Distributed Systems
 
 Spring 2014
 
 Instructor:  Hai Jiang
 
 Assignment # 1
 
 Due Date:    Tuesday, Mar. 04, 2014
 
 ≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈
 Description: This program tries to solve Dining Philospher problem.
 
 To achieve the goal, the program uses following features:
 
 + pthread ------------------------ To simultaneously listen to broadcast messages, listen to neighbor and dining.
 + TCP/IP socket programming ------ Neighbor Intereaction
 + UDP socket --------------------- broadcasting messaging
 + pthread mutex ------------------ To avoid concurrent access to resources (inconsistancy). Try Lock Approach.
 + Fault Tolerance ---------------- If any of the machine stops working in between. The neighbor machines makes appropriate adjustments.
 + Node Restoration --------------- A node which somehow went out of the plan, can join anytime.
 + Signal Handling ---------------- Listens to signals like CTRL+c(SIGINT) and CTRL+z(SIGTSTP) to start and stop dining, respectively. And also to broadcast respective message
 + Configuartion File ------------- Read the configurations like port number and neighbor details from the configuration file. Hence gives the capability to start with any desired number of philosophers.
 + Monitor ------------------------ To monitor the status of all the nodes present in the plan.
 + Heartbeat ---------------------- thread added to help the monitor to monitor the node.
 
 ≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈
 Dealing with common issues:
 
 Deadlocks ------------------------ To deal with situations like deadlocks, the philospher drops the first chopstick if the seconds chopstick is not available.
 
 Starvation ----------------------- The last machine first asks the neighbor for chopstick, rather than trying to get the local chopstick, as the other machines do.
 
 ≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈≈
 Editor/Platform:  Xcode/Linux
 
 Compile:    gcc dining.c -o dining
 
 Command:
 
 to run	-							dining [-d] -cn val
 
 -d	--> runs the program in debug mode. Shows detail log information.
 
 -cn --> must argument. Tell the program to adopt a particular confgration from the configration file.
 val --> Value comes after the -cn, is index of a particular configration.
 
 to start dining					CTRL-c
 
 to stop dining(exit)				CTRL-z
 
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <string.h>

#include <pthread.h>
#include <signal.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "NodesManager.c"

typedef int socketDescriptor;

#define Interface "venet0:0"
#define Filename "config.txt"

#define MESSAGESIZE sizeof(Message)

#define MSG_FLAG 0x0

RunningMode runningMode;
NodesManager nodesManager;
Node *localNode;
LocalData localData;
unsigned int newLeftNeighborIPIndex;
pthread_mutex_t chopstickMutex = PTHREAD_MUTEX_INITIALIZER;

/*----------------------------------------------------------------
 Display Status and Error Messages on console.
 ----------------------------------------------------------------*/
void display(char *argv, DisplayType displayType);

/*----------------------------------------------------------------
 Display Dining Status on console
 ----------------------------------------------------------------*/
void displayDiningStatus(DiningStatus diningStatus);

/*----------------------------------------------------------------
 Set Dining Status the dining status and call the appr. method to
 display it.
 ----------------------------------------------------------------*/
void setDiningStatus(DiningStatus diningStatus);

/*----------------------------------------------------------------
 Initialize Variables like nodesManager, localNode, informs right 
 neighbor for its presense.
 ----------------------------------------------------------------*/
void initializeVariables(unsigned int configID);

/*----------------------------------------------------------------
 Destroy Variables like chopstickMutex
 ----------------------------------------------------------------*/
void destroyVariablesAndExit();

/*----------------------------------------------------------------
 Handle the Signal CTRL+C & CTRL+Z
 ----------------------------------------------------------------*/
void sig_handler(int signo);

/*----------------------------------------------------------------
 Broadcasting Start and Stop Signals
 ----------------------------------------------------------------*/
void broadcastSignalToNode(Node *node, SignalType st);

/*----------------------------------------------------------------
 This function is based upon pthread mutex try lock mechanism.
 First checks if the current status of the chopstick matches the
 first parameter passed, to ensure that only the appropriate
 thread can change the status of the chopstick.
 Return true, if successful, false otherwise.
 ----------------------------------------------------------------*/
bool changeChopstickStatus(ChopStickStatus currentStatus, ChopStickStatus statusRequired);

/*----------------------------------------------------------------
 Tries to set the access to first chopstick with desired status.
 isOdd decides the first chopstick (either local or remote)
 Return true, if successful, false otherwise.
 ----------------------------------------------------------------*/
bool getFirstChopstick();

/*----------------------------------------------------------------
 Tries to set the access to second chopstick with desired status.
 isOdd decides the second chopstick (either local or remote)
 Return true, if successful, false otherwise.
 ----------------------------------------------------------------*/
bool getSecondChopstick();

/*----------------------------------------------------------------
 Tries to set the access to second chopstick with desired status.
 isOdd decides the second chopstick (either local or remote)
 ----------------------------------------------------------------*/
void releaseSecondChopstick();

/*----------------------------------------------------------------
 Tries to set the access to first chopstick with desired status.
 isOdd decides the first chopstick (either local or remote)
 ----------------------------------------------------------------*/
void releaseFirstChopstick();

/*----------------------------------------------------------------
 contact neighbor for the Chopstick, to ask for the dining status 
 and inform about its own presense.
 Input Parameter-->
 SignalTypeCPReleased: Ask the neighbor to set its chopstick free
 SignalTypeCPRequest: Ask neighbor for chopstick,
 return true, if given, false otherwise.
 SignalTypeStatusRequest: Request the status so that it can configure 
 its self accordingly.
 ----------------------------------------------------------------*/
bool synchronizeWithNeighbor(SignalType signalType);

/*----------------------------------------------------------------
 Check if neighbor, holding the local chopstick is up/reachable
 return true, if yes, false otherwise.
 ----------------------------------------------------------------*/
bool isRightNeighborReachable();

/*----------------------------------------------------------------
 Check if the index of node passed capable of being the new left 
 neighbor.
 ----------------------------------------------------------------*/
bool canNodeBeNewLeftNeighbor(int index);

/*----------------------------------------------------------------
 Listening to neighbor for requests like SignalTypeStatusRequest,
 SignalTypeCPRequest and SignalTypeCPReleased
 Return true, upon SignalTypeCPRequest, if chopstick is given.
 false otherwise.
 Upon receiving the signal "SignalTypeStatusRequest", it sends its
 local dining status and configure itself whereever needed.
 ----------------------------------------------------------------*/
void *listenToNeighbor(void *argv);

/*----------------------------------------------------------------
 Listening to request thats asks for current status of node
 ----------------------------------------------------------------*/
void *heartbeat(void *argv);

/*----------------------------------------------------------------
 Start the dining thread
 ----------------------------------------------------------------*/
void startDining();

/*----------------------------------------------------------------
 Dining process: Thinking, Eating, Dropping ChopSticks
 (checks if new neighbor is available)
 1. Thinks for a random time
 2. Try to pick first chopstick, on success continue otherwise
 go to step 1
 3. Try to pick second chopstick, on sucess continue otherwise
 go to step 6
 4. Start Eating
 5. Drops second chopstick
 6. Drops first chopstick
 7. go to step 1.
 ----------------------------------------------------------------*/
void* dining(void *arg);

/*----------------------------------------------------------------
 1. Initilize variables
 2. Register for Signals
 3. Start listening to broadcast message
 ----------------------------------------------------------------*/
int main(int argc, char **argv)
{
	pthread_t listenTNThread, heartbeatThread;
	runningMode = RunningModeRelease;
	
	socketDescriptor localSocketDes;
//	int optionValue = 1;
	int i;
	struct sockaddr_in localSocket, remoteSocket;
	socklen_t socketLength = sizeof(struct sockaddr_in);
	
	bool configFound = false;
	unsigned int configID;
	
	for (i = 1; i<argc;i++)
	{
		if(!strcmp("-d", argv[i]))
		{
			runningMode = RunningModeDebug;
			display("[Debug Mode]", DisplayTypeStatus);
		}
		else if (!strcmp("-cn", argv[i]))
		{
			configFound = true;
			if (++i < argc)
				configID = atoi(argv[i]);
			else
				display("Invalid number of arguments (-cn value missing)", DisplayTypeError);
		}
	}
	
	if (!configFound)
		display("Arguments Insufficient(-cn)", DisplayTypeError);
	
	if (signal(SIGINT, sig_handler) == SIG_ERR)
		display("Can't catch SIGINT", DisplayTypeError);
	
	if (signal(SIGTSTP, sig_handler) == SIG_ERR)
		display("Can't catch SIGTSTP", DisplayTypeError);
	
	initializeVariables(configID);
	
	if (pthread_create(&(listenTNThread), NULL, &listenToNeighbor, NULL) != 0)
		display("Can't create listenToNeighbor thread", DisplayTypeError);
	
	if (pthread_create(&(heartbeatThread), NULL, &heartbeat, NULL) != 0)
		display("Can't create heartbeat thread", DisplayTypeError);
	
	if ((localSocketDes = socket(AF_INET, SOCK_DGRAM, PF_UNSPEC)) == ERROR)
		display("Socket", DisplayTypeError);
	
	bzero(&localSocket, socketLength);
	localSocket.sin_family = AF_INET;
	localSocket.sin_addr.s_addr = INADDR_ANY;
	localSocket.sin_port = htons(localNode->broadCastPort);
	
//	if ((setsockopt(localSocketDes, SOL_SOCKET, SO_REUSEADDR, &optionValue, sizeof(optionValue))) == ERROR)
//		display("main Setsockopt", DisplayTypeError);
	
	if (bind(localSocketDes, (struct sockaddr *) & localSocket, socketLength) == ERROR)
		display("Bind", DisplayTypeError);
	
	Message msg;
	
	while (true)
	{
		if (recvfrom(localSocketDes, &msg, MESSAGESIZE, MSG_FLAG, (struct sockaddr *) &remoteSocket, &socketLength)<0)
			display("Receiving Broadcast Message", DisplayTypeError);
		
		SignalType signalType = msg.signalType;
		
		switch (signalType)
		{
			case SignalTypeStart:
				display("SignalTypeStart", DisplayTypeStatus);
				startDining();
				break;
				
			case SignalTypeStop:
				display("SignalTypeStop", DisplayTypeStatus);
				destroyVariablesAndExit();
				break;
				
			default:
				display("Broadcast Receiver received unexpected signal", DisplayTypeError);
				break;
		}
	}
	close(localSocketDes);
	return 0;
}

#pragma Chopstick handling

bool changeChopstickStatus(ChopStickStatus currentStatus, ChopStickStatus statusRequired)
{
	bool flag = false;
	
	if (!pthread_mutex_trylock(&chopstickMutex)) {
		if (localData.chopStickStatus == currentStatus ||
			localData.chopStickStatus == statusRequired ||
			(localData.chopStickStatus == ChopstickUsedRemote &&
			 statusRequired == ChopstickUsedLocal &&
			 !isRightNeighborReachable())) {
				localData.chopStickStatus = statusRequired;
				flag = true;
			}
		pthread_mutex_unlock(&chopstickMutex);
	}
	else
		display("Chopstick Mutex: Couldn't get the lock", DisplayTypeStatus);
	
	return flag;
}

bool getFirstChopstick()
{
	return localData.isOdd ? synchronizeWithNeighbor(SignalTypeCPRequest) : changeChopstickStatus(ChopstickFree, ChopstickUsedLocal);
}

bool getSecondChopstick()
{
	return localData.isOdd ? changeChopstickStatus(ChopstickFree, ChopstickUsedLocal) : synchronizeWithNeighbor(SignalTypeCPRequest);
}

void releaseSecondChopstick()
{
	localData.isOdd ? changeChopstickStatus(ChopstickUsedLocal, ChopstickFree) : synchronizeWithNeighbor(SignalTypeCPReleased);
}

void releaseFirstChopstick()
{
	localData.isOdd ? synchronizeWithNeighbor(SignalTypeCPReleased) : changeChopstickStatus(ChopstickUsedLocal, ChopstickFree);
}

#pragma Console Handling

void display(char *msg, DisplayType displayType)
{
	if (runningMode == RunningModeRelease && displayType == DisplayTypeStatus)
		return;
	
	if (displayType != DisplayTypeError)
	{
		fprintf(stdout, "%s\n" , msg);
		fflush(stdout);
	}
	else
	{
		perror(msg);
		destroyVariablesAndExit();
	}
}

void displayDiningStatus(DiningStatus diningStatus)
{
	DisplayType displayType = DisplayTypeStatus;
	char msg[100];
	switch (diningStatus)
	{
		case DiningStatusWaiting:
			sprintf(msg, "Philospher %d is waiting", localData.localIPIndex);
			displayType = DisplayTypeStatusRelease;
			break;
		case DiningStatusWaiting_:
			sprintf(msg, "Philospher %d is waiting *", localData.localIPIndex);
			displayType = DisplayTypeStatusRelease;
			break;
		case DiningStatusStartedDining:
			sprintf(msg, "Philospher %d started dining", localData.localIPIndex);
			break;
		case DiningStatusThinking:
			sprintf(msg, "Philospher %d is thinking", localData.localIPIndex);
			displayType = DisplayTypeStatusRelease;
			break;
		case DiningStatusFirstChopStickPicked:
			sprintf(msg, "Philospher %d picked first chopstick", localData.localIPIndex);
			break;
		case DiningStatusSecondChopStickPicked:
			sprintf(msg, "Philospher %d picked second chopstick", localData.localIPIndex);
			break;
		case DiningStatusEating:
			sprintf(msg, "Philospher %d is eating", localData.localIPIndex);
			displayType = DisplayTypeStatusRelease;
			break;
		case DiningStatusDroppingBothChopSticks:
			sprintf(msg, "Philospher %d done eating for the round, will now drop both chopsticks", localData.localIPIndex);
			break;
		case DiningStatusSCPNADroppingFirstChopStick:
			sprintf(msg, "second chopstick not available. Philospher %d will drop first chopstick and think now.", localData.localIPIndex);
			break;
		case DiningStatusFirstChopStickNA:
			sprintf(msg, "first chopstick not available. Philospher %d will think now.", localData.localIPIndex);
			break;
			
		default:
			msg[0] = '\0';
			break;
	}
	display(msg, displayType);
}

void setDiningStatus(DiningStatus diningStatus)
{
	if (localData.diningStatus == DiningStatusWaiting || localData.diningStatus == DiningStatusWaiting_)
	{
		/* In order to avoid inconsistancy after the node stays alone*/
		if(diningStatus == DiningStatusStartedDining)
			localData.diningStatus = DiningStatusStartedDining;
	}
	else
		localData.diningStatus = diningStatus;
	displayDiningStatus(localData.diningStatus);
}

#pragma Configurations

char *localIP()
{
	socketDescriptor localSocketDes;
	struct ifreq ifr;
	localSocketDes = socket(AF_INET, SOCK_DGRAM, PF_UNSPEC);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, Interface, IFNAMSIZ-1);
	ioctl(localSocketDes, SIOCGIFADDR, &ifr);
	close(localSocketDes);
	return inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
}

void initializeVariables(unsigned int configID)
{
	if (!initNodesManager(&nodesManager, Filename))
		display("Cant open Config File", DisplayTypeError);
	
	if (configID < nodesManager.used)
	{
		localNode = nodesManager.nodeList[configID];
		if (strcmp(localNode->ipAddr, localIP()))
			display("Invalid Configration Supplied. Local IP doesn't match", DisplayTypeError);
	}
	else
		display("Configuration file doesn't include the configration supplied", DisplayTypeError);
	
	localData.localIPIndex = configID;
	localData.leftNeighborIPIndex = (localData.localIPIndex + 1) % nodesManager.used;
	
	if (localData.localIPIndex == localData.leftNeighborIPIndex)
		display("Only one machine as per config file, thus can't proceed. will exit", DisplayTypeError);
	
	localData.isOdd = localData.leftNeighborIPIndex <= localData.localIPIndex;
	
	/*----------------------------------------------------------------
	 No need if the local node has valid right neighbor.
	 Reason: A valid left neighbor implies that a valid right neighbor.(round table)
	 ----------------------------------------------------------------*/
	localData.rightNeighborIPIndex = (localData.localIPIndex - 1) % nodesManager.used;
	localData.chopStickStatus = ChopstickFree;
	
	display("Press CTRL+c to begin\n      CTRL+z to stop\n", DisplayTypeStatusRelease);
	srand(getpid());
	newLeftNeighborIPIndex = ERROR;
	
	synchronizeWithNeighbor(SignalTypeStatusRequest);
}

void destroyVariablesAndExit()
{
	newLeftNeighborIPIndex = ERROR;
	localNode = NULL;
	localData = emptyData;
	pthread_mutex_destroy(&chopstickMutex);
	freeNodesManager(&nodesManager);
	exit(0);
}

#pragma Broadcasting

void sig_handler(int signo)
{
	SignalType signalType= (signo == SIGINT) ? SignalTypeStart : SignalTypeStop;
	
	if (signalType == SignalTypeStart && localData.diningStatus != DiningStatusWaiting)
		return;
	
	int i;
	for (i = 0; i < nodesManager.used; i++)
		if ( i != localData.localIPIndex)
			broadcastSignalToNode(nodesManager.nodeList[i], signalType);
	
	display("", DisplayTypeStatusRelease);
	
	if (signalType == SignalTypeStart)
	{
		display("\nSignalTypeStart", DisplayTypeStatus);
		startDining();
	}
	else
	{
		display("\nSignalTypeStop", DisplayTypeStatus);
		destroyVariablesAndExit();
	}
}

void broadcastSignalToNode(Node *remoteNode, SignalType signalType)
{
	socketDescriptor remoteSocketDes;
	struct sockaddr_in remote_socket;
	socklen_t socketLength = sizeof(struct sockaddr_in);
	
	if ((remoteSocketDes = socket(AF_INET, SOCK_DGRAM, PF_UNSPEC)) == ERROR)
		display("Socket", DisplayTypeError);
	
	Message msg = {signalType, localData.localIPIndex};
	
	bzero(&remote_socket, socketLength);
	remote_socket.sin_family = AF_INET;
	remote_socket.sin_addr.s_addr = inet_addr(remoteNode->ipAddr);
	remote_socket.sin_port = htons(remoteNode->broadCastPort);
	
	if (sendto(remoteSocketDes, &msg, MESSAGESIZE, MSG_FLAG, (struct sockaddr *)&remote_socket, socketLength)<0)
		display("broadcastSignalToNode", DisplayTypeError);
	close(remoteSocketDes);
}

#pragma Neighbor Handling

bool synchronizeWithNeighbor(SignalType signalType)
{
	bool reconnectFlag = true, connectionSuccess = false, gotChopstick = false, tryAgain = true;
	socketDescriptor remoteSocketDes;
	struct sockaddr_in remoteSocket;
	socklen_t socketLength = sizeof(struct sockaddr_in);
	
	if ((remoteSocketDes = socket(AF_INET, SOCK_STREAM, PF_UNSPEC)) == ERROR)
		display("Socket", DisplayTypeError);
	
	if (signalType == SignalTypeStatusRequest)
	{
		while (reconnectFlag)
		{
			bzero(&remoteSocket, socketLength);
			remoteSocket.sin_family = AF_INET;
			remoteSocket.sin_addr.s_addr = inet_addr(nodesManager.nodeList[localData.rightNeighborIPIndex]->ipAddr);
			remoteSocket.sin_port = htons(nodesManager.nodeList[localData.rightNeighborIPIndex]->chopstickRequestPort);
			
			if ((connect(remoteSocketDes, (struct sockaddr *) &remoteSocket, socketLength)) == ERROR)
			{
				if (tryAgain)
				{
					tryAgain = !tryAgain;
				}
				else
				{
					if (errno == EHOSTUNREACH || errno == ECONNREFUSED)
					{
						localData.rightNeighborIPIndex = (localData.rightNeighborIPIndex - 1) % nodesManager.used;
						if (localData.rightNeighborIPIndex == localData.localIPIndex)
						{
							//the very first machine will reach here
							setDiningStatus(DiningStatusWaiting);
							display("First machine. Will wait for others to join.", DisplayTypeStatusRelease);
							reconnectFlag = connectionSuccess = false;
						}
						tryAgain = true;
					}
					else
						display("connect: synchronizeWithNeighbor", DisplayTypeError);
				}
			}
			else
			{
				reconnectFlag = false;
				connectionSuccess = true;
			}
		}
	}
	else
	{
		while (reconnectFlag)
		{
			bzero(&remoteSocket, socketLength);
			remoteSocket.sin_family = AF_INET;
			remoteSocket.sin_addr.s_addr = inet_addr(nodesManager.nodeList[localData.leftNeighborIPIndex]->ipAddr);
			remoteSocket.sin_port = htons(nodesManager.nodeList[localData.leftNeighborIPIndex]->chopstickRequestPort);
			
			if ((connect(remoteSocketDes, (struct sockaddr *) &remoteSocket, socketLength)) == ERROR)
			{
				if (tryAgain)
				{
					tryAgain = !tryAgain;
				}
				else
				{
					if (errno == EHOSTUNREACH || errno == ECONNREFUSED)
					{
						localData.leftNeighborIPIndex = (localData.leftNeighborIPIndex + 1) % nodesManager.used;
						if (localData.leftNeighborIPIndex == localData.localIPIndex)
						{
							setDiningStatus(DiningStatusWaiting_);
							display("This is only machine left in the plan. Will wait for others to join.", DisplayTypeStatusRelease);
							reconnectFlag = connectionSuccess = false;
						}
						else if (signalType == SignalTypeCPReleased)
							reconnectFlag = connectionSuccess = false;
						tryAgain = true;
						
						localData.isOdd = localData.leftNeighborIPIndex <= localData.localIPIndex;
						display("Left neighbor changed", DisplayTypeStatusRelease);
					}
					else
						display("connect: synchronizeWithNeighbor", DisplayTypeError);
				}
			}
			else
			{
				reconnectFlag = false;
				connectionSuccess = true;
			}
		}
	}
	
	if (connectionSuccess)
	{
		Message msg = {signalType, localData.localIPIndex};
		
		if (send(remoteSocketDes, &msg, MESSAGESIZE, MSG_FLAG) < MESSAGESIZE)
			display("Sending message to neighbor", DisplayTypeError);
		
		if (signalType != SignalTypeCPReleased)
		{
			if (recv(remoteSocketDes, &msg, MESSAGESIZE, MSG_FLAG) < 1)
				display("receiving message from neighbor", DisplayTypeError);
			
			SignalType signalType = msg.signalType;
			
			switch (signalType) {
				case SignalTypeCPRequestFulfilled:
					display("SignalTypeCPRequestFulfilled", DisplayTypeStatus);
					gotChopstick = true;
					break;
					
				case SignalTypeCPRequestDenied:
					display("SignalTypeCPRequestDenied", DisplayTypeStatus);
					break;
					
				case SignalTypeStatusWaiting:
					//Start signal from user, not received yet
					display("SignalTypeStatusWaiting", DisplayTypeStatus);
					setDiningStatus(DiningStatusWaiting);
					break;
					
				case SignalTypeStatusWaiting_:
					//There is one node, which is waiting for others to join.
					display("SignalTypeStatusWaiting_", DisplayTypeStatus);
					startDining();
					break;
					
				case SignalTypeStatusDining:
					//Node is dining.
					display("SignalTypeStatusDining", DisplayTypeStatus);
					startDining();
					break;
					
				default:
					display("Chopstick Request sender received unexpected signal", DisplayTypeError);
					break;
			}
		}
	}
	
	close(remoteSocketDes);
	return gotChopstick;
}

bool isRightNeighborReachable()
{
	bool flag = true;
	socketDescriptor remoteSocketDes;
	struct sockaddr_in remoteSocket;
	socklen_t socketLength = sizeof(struct sockaddr_in);
	
	if ((remoteSocketDes = socket(AF_INET, SOCK_STREAM, PF_UNSPEC)) == ERROR)
		display("Socket", DisplayTypeError);
	
	bzero(&remoteSocket, socketLength);
	remoteSocket.sin_family = AF_INET;
	remoteSocket.sin_addr.s_addr = inet_addr(nodesManager.nodeList[localData.rightNeighborIPIndex]->ipAddr);
	remoteSocket.sin_port = htons(nodesManager.nodeList[localData.rightNeighborIPIndex]->chopstickRequestPort);
	
	if ((connect(remoteSocketDes, (struct sockaddr *) &remoteSocket, socketLength)) == ERROR)
	{
		if (errno == EHOSTUNREACH || errno == ECONNREFUSED) {
			flag = false;
			display("Right Neighbor not reachable", DisplayTypeStatusRelease);
		}
	}
	
	close(remoteSocketDes);
	return flag;
}

bool canNodeBeNewLeftNeighbor(int index)
{
	bool flag = false;
	
	if (index >= 0 && index < nodesManager.used)
	{
		if (localData.localIPIndex == localData.leftNeighborIPIndex)
			flag = true;
		else if(index == localData.leftNeighborIPIndex)
			flag = true;
		else if (index > localData.localIPIndex)
		{
			if ((localData.leftNeighborIPIndex > localData.localIPIndex && index < localData.leftNeighborIPIndex) ||
				(localData.leftNeighborIPIndex < localData.localIPIndex))
				flag = true;
		}
		else if(index < localData.leftNeighborIPIndex && index < localData.localIPIndex)
		{
			flag = true;
		}
	}
	
	return flag;
}

void *listenToNeighbor(void *argv)
{
	socketDescriptor localSocketDes, remoteSocketDes;
	struct sockaddr_in localSocket, remoteSocket;
	socklen_t socketLength = sizeof(struct sockaddr_in);
//	int optionValue = 1;
	Message msg;
	
	if ((localSocketDes = socket(AF_INET, SOCK_STREAM, PF_UNSPEC)) == ERROR)
		display("Socket", DisplayTypeError);
	
	bzero(&localSocket, socketLength);
	localSocket.sin_family = AF_INET;
	localSocket.sin_addr.s_addr = INADDR_ANY;
	localSocket.sin_port = htons(localNode->chopstickRequestPort);
	
//	if ((setsockopt(localSocketDes, SOL_SOCKET, SO_REUSEADDR, &optionValue, sizeof(optionValue))) == ERROR)
//		display("Setsockopt", DisplayTypeError);
	
	if (bind(localSocketDes, (struct sockaddr *) &localSocket, socketLength) == ERROR)
		display("Bind", DisplayTypeError);
	
	if ((listen(localSocketDes, SOMAXCONN)) == ERROR)
		display("Listen", DisplayTypeError);
	
	long count = 0;
	while (true)
	{
		if ((remoteSocketDes = accept(localSocketDes, (struct sockaddr *) &remoteSocket, &socketLength)) == ERROR)
			display("Accept", DisplayTypeError);
		
		count = recv(remoteSocketDes, &msg, MESSAGESIZE, MSG_FLAG);
		
		if (count < 0)
			display("receiving message from neighbor", DisplayTypeError);
		else if (count)
		{
			SignalType signalType = msg.signalType;
			
			switch (signalType)
			{
				case SignalTypeCPRequest:
					display("SignalTypeCPRequest", DisplayTypeStatus);
					
					if (changeChopstickStatus(ChopstickFree, ChopstickUsedRemote))
					{
						localData.rightNeighborIPIndex = msg.nodeIndex;
						msg.signalType = SignalTypeCPRequestFulfilled;
					}
					else
						msg.signalType = SignalTypeCPRequestDenied;
					
					msg.nodeIndex = localData.localIPIndex;
					
					if (send(remoteSocketDes, &msg, MESSAGESIZE, MSG_FLAG) < 1)
						display("Send in listenToNeighbor", DisplayTypeError);
					break;
					
				case SignalTypeCPReleased:
					display("SignalTypeCPReleased", DisplayTypeStatus);
					changeChopstickStatus(ChopstickUsedRemote, ChopstickFree);
					break;
					
				case SignalTypeStatusRequest:
				{
					display("SignalTypeStatusRequest", DisplayTypeStatus);
					
					int index = msg.nodeIndex;
					
					switch (localData.diningStatus)
					{
						case DiningStatusWaiting:
							msg.signalType = SignalTypeStatusWaiting;
							localData.leftNeighborIPIndex = canNodeBeNewLeftNeighbor(index) ? index : localData.leftNeighborIPIndex;
							localData.isOdd = localData.leftNeighborIPIndex <= localData.localIPIndex;
							break;
							
						case DiningStatusWaiting_:
							msg.signalType = SignalTypeStatusWaiting_;
							localData.leftNeighborIPIndex = canNodeBeNewLeftNeighbor(index) ? index : localData.leftNeighborIPIndex;
							localData.isOdd = localData.leftNeighborIPIndex <= localData.localIPIndex;
							startDining();
							break;
							
						default:
							newLeftNeighborIPIndex = canNodeBeNewLeftNeighbor(index)? index : ERROR;
							msg.signalType = SignalTypeStatusDining;
							break;
					}
					
					msg.nodeIndex = localData.localIPIndex;
					
					if (send(remoteSocketDes, &msg, MESSAGESIZE, MSG_FLAG) < 1)
						display("Send in listenToNeighbor", DisplayTypeError);
					break;
				}
				default:
					display("Chopstick Request receiver received unexpected signal", DisplayTypeError);
					break;
			}
		}
		close(remoteSocketDes);
	}
	close(localSocketDes);
	return NULL;
}

void *heartbeat(void *argv)
{
//	int optionValue = 1;
	socketDescriptor localSocketDes, remoteSocketDes;
	struct sockaddr_in localSocket, remoteSocket;
	socklen_t socketLength = sizeof(struct sockaddr_in);
	
	if ((localSocketDes = socket(AF_INET, SOCK_STREAM, PF_UNSPEC)) == ERROR)
		display("Socket", DisplayTypeError);
	
	bzero(&localSocket, socketLength);
	localSocket.sin_family = AF_INET;
	localSocket.sin_addr.s_addr = INADDR_ANY;
	localSocket.sin_port = htons(localNode->heartbeatPort);
	
//	if ((setsockopt(localSocketDes, SOL_SOCKET, SO_REUSEADDR, &optionValue, sizeof(optionValue))) == ERROR)
//		display("setsockopt", DisplayTypeError);
	
	if (bind(localSocketDes, (struct sockaddr *) &localSocket, socketLength) == ERROR)
		display("Bind", DisplayTypeError);
	
	if ((listen(localSocketDes, SOMAXCONN)) == ERROR)
		display("Listen", DisplayTypeError);
	
	while (true)
	{
		if ((remoteSocketDes = accept(localSocketDes, (struct sockaddr *) &remoteSocket, &socketLength)) == ERROR)
			display("Accept", DisplayTypeError);
		if (send(remoteSocketDes, &localData, sizeof(localData), MSG_FLAG) < 1)
			display("Send in heartbeat", DisplayTypeError);
		close(remoteSocketDes);
	}
	close(localSocketDes);
	return NULL;
}

#pragma dining

void startDining()
{
	if (localData.diningStatus == DiningStatusWaiting || localData.diningStatus == DiningStatusWaiting_)
	{
		pthread_t diningThread;
		if (pthread_create(&(diningThread), NULL, &dining, NULL) != 0)
			display("Can't create dining thread", DisplayTypeError);
	}
}

void* dining(void *arg)
{
	struct timespec delayTime;
	setDiningStatus(DiningStatusStartedDining);
	while (localData.diningStatus != DiningStatusWaiting_)
	{
		setDiningStatus(DiningStatusThinking);
		if (newLeftNeighborIPIndex != ERROR)
		{
			display("Left neighbor changed", DisplayTypeStatusRelease);
			localData.leftNeighborIPIndex = newLeftNeighborIPIndex;
			localData.isOdd = localData.leftNeighborIPIndex <= localData.localIPIndex;
			newLeftNeighborIPIndex = ERROR;
		}
		
		delayTime = delay();
		nanosleep(&delayTime, NULL);
		if (getFirstChopstick())
		{
			setDiningStatus(DiningStatusFirstChopStickPicked);
			if (getSecondChopstick())
			{
				setDiningStatus(DiningStatusSecondChopStickPicked);
				
				setDiningStatus(DiningStatusEating);
				delayTime = delay();
				nanosleep(&delayTime, NULL);
				
				setDiningStatus(DiningStatusDroppingBothChopSticks);
				releaseSecondChopstick();
			}
			else
				setDiningStatus(DiningStatusSCPNADroppingFirstChopStick);
			releaseFirstChopstick();
		}
		else
			setDiningStatus(DiningStatusFirstChopStickNA);
	}
	return NULL;
}