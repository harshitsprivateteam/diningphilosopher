/********************************************************************
 
 ***** Monitor.c *****
 
 Computer Science 6823 Distributed Systems
 
 Spring 2014
 
 Instructor:  Hai Jiang
 
 Assignment # 1
 
 Due Date:    Tuesday, Mar. 04, 2014
 
 Description:	This program monitors all the nodes in dining philospher
 
 *******************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include "NodesManager.c"

#define Filename "config.txt"
#define OutFilename "Output.log"

#define TotalColumns 6
#define ColumnWidth 20
#define TotalWidth ColumnWidth*TotalColumns + TotalColumns + 3

NodesManager nodesManager;
char **printString;
pthread_mutex_t outputFileMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t printMutex= PTHREAD_MUTEX_INITIALIZER;

char * Line(char strings[][ColumnWidth]);

/*----------------------------------------------------------------
 Destroy all the initialized variables.
 ----------------------------------------------------------------*/
void destroyVariablesAndExit()
{
	int i;
    for(i = 0; i <= nodesManager.used; i++)
		free(printString[i]);
	
	free(printString);
	pthread_mutex_destroy(&outputFileMutex);
	pthread_mutex_destroy(&printMutex);
	freeNodesManager(&nodesManager);
	exit(0);
}

/*----------------------------------------------------------------
 Display Error Messages on console and exits.
 ----------------------------------------------------------------*/
void display(char *msg, DisplayType displayType)
{
	perror(msg);
	destroyVariablesAndExit();
}

/*----------------------------------------------------------------
 First, take the writing head to proper position
 and the write the msg at the appropriate position.
 ----------------------------------------------------------------*/
void print(char *message, char *positionSpecifier)
{
	if (!pthread_mutex_lock(&printMutex)) /* using lock not being sure if stdout is thread-safe or not*/
	{
		printf("%s", positionSpecifier);               /* setting the cursor position along y-axis*/
		printf("%s", message);                /* printing the string from the starting of the line*/
		fflush(stdout);
		pthread_mutex_unlock(&printMutex);
	}
}

/*----------------------------------------------------------------
 Logs activity of all the nodes recorded.
 ----------------------------------------------------------------*/
void updateFile()
{
	if (!pthread_mutex_trylock(&outputFileMutex)) {
		FILE *outputFD = fopen(OutFilename, "a+");
		if (outputFD == NULL) {
			perror("outputFD");
		}
		else
		{
			int i;
			for (i = 0; i <= nodesManager.used; i++) {
				fputs(printString[i], outputFD);
				fputs("\n", outputFD);
			}
			fclose(outputFD);
		}
		pthread_mutex_unlock(&outputFileMutex);
	}
}

/*----------------------------------------------------------------
 This is a thread based method. which asks the neighbor for its 
 status and logs/prints corresponding data received.
 ----------------------------------------------------------------*/
void *getNodeStatus(void *argv)
{
	int threadNumber = *((int*)argv);
	Node *node = nodesManager.nodeList[threadNumber];
	
	int socketDescriptor;
	struct sockaddr_in remoteSocket;
	int dataSize = sizeof(LocalData);
	LocalData localData;
	char positionSpecifier[ColumnWidth];
	char strings[TotalColumns][ColumnWidth];
	char temp[3];
	
	while (1)
	{
		localData = emptyData;
		
		if ((socketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) != ERROR)
		{
			remoteSocket.sin_family = AF_INET;
			remoteSocket.sin_port = htons(node->heartbeatPort);
			remoteSocket.sin_addr.s_addr = inet_addr(node->ipAddr);
			bzero(&remoteSocket.sin_zero, 8);
			
			if ((connect(socketDescriptor, (struct sockaddr *) &remoteSocket, sizeof(struct sockaddr_in))) != ERROR)
			{
				if(recv (socketDescriptor, &localData, dataSize, 0) < 1)
				{
					//			perror("recv");
					//			pthread_exit(NULL);
				}
			}
			else
			{
				//			perror("connect");
				//			pthread_exit(NULL);
			}
			
		}
		else
		{
			//			perror("socket");
			//			pthread_exit(NULL);
		}
		
		
		if (localData.localIPIndex == localData.leftNeighborIPIndex &&
			localData.localIPIndex == localData.rightNeighborIPIndex)
		{
			sprintf(temp, "%d", threadNumber);
			strncpy(strings[0], temp, ColumnWidth);
			strncpy(strings[1], "---", ColumnWidth);
			strncpy(strings[2], "---", ColumnWidth);
			strncpy(strings[3], "---", ColumnWidth);
			strncpy(strings[4], "---", ColumnWidth);
			strncpy(strings[5], "---", ColumnWidth);
		}
		else
		{
			sprintf(temp, "%d", localData.localIPIndex);
			strncpy(strings[0], temp, ColumnWidth);
			sprintf(temp, "%d", localData.leftNeighborIPIndex);
			strncpy(strings[1], temp, ColumnWidth);
			
			sprintf(temp, "%d", localData.rightNeighborIPIndex);
			strncpy(strings[2], temp, ColumnWidth);
			
			switch (localData.chopStickStatus) {
				case ChopstickFree:
					strncpy(strings[3], "Free", ColumnWidth);
					break;
					
				case ChopstickUsedLocal:
					strncpy(strings[3], "Local", ColumnWidth);
					break;
					
				case ChopstickUsedRemote:
					strncpy(strings[3], "Remote", ColumnWidth);
					break;
					
				default:
					strncpy(strings[3], "---", ColumnWidth);
					break;
			}
			
			switch (localData.diningStatus) {
				case DiningStatusWaiting:
					strncpy(strings[4], "Waiting", ColumnWidth);
					break;
					
				case DiningStatusWaiting_:
					strncpy(strings[4], "Waiting *", ColumnWidth);
					break;
					
				case DiningStatusStartedDining:
					strncpy(strings[4], "Started Dining", ColumnWidth);
					break;
					
				case DiningStatusEating:
					strncpy(strings[4], "Eating", ColumnWidth);
					break;
					
				case DiningStatusThinking:
					strncpy(strings[4], "Thinking", ColumnWidth);
					break;
					
				case DiningStatusDroppingBothChopSticks:
					strncpy(strings[4], "Dropping Both", ColumnWidth);
					break;
					
				case DiningStatusFirstChopStickNA:
					strncpy(strings[4], "First NA", ColumnWidth);
					break;
					
				case DiningStatusFirstChopStickPicked:
					strncpy(strings[4], "First Picked", ColumnWidth);
					break;
					
				case DiningStatusSCPNADroppingFirstChopStick:
					strncpy(strings[4], "Dropping First", ColumnWidth);
					break;
					
				case DiningStatusSecondChopStickPicked:
					strncpy(strings[4], "Second Picked", ColumnWidth);
					break;
					
				default:
					strncpy(strings[4], "---", ColumnWidth);
					break;
			}
			strncpy(strings[5], localData.isOdd ? "*" : " ", ColumnWidth);
		}
		close(socketDescriptor);
		strncpy(printString[threadNumber + 1], Line(strings), TotalWidth);
		sprintf(positionSpecifier, "\x1B[%d;0f",threadNumber + 2);
//		updateFile();
		print(printString[threadNumber + 1], positionSpecifier);
		sleep(1);
	}
	return NULL;
}

/*----------------------------------------------------------------
 Create a message to be displayed.
 ----------------------------------------------------------------*/
char * Line(char strings[][ColumnWidth])
{
	char *line = (char *) calloc(TotalWidth, sizeof(char));
	
	int noOfSpaces = ColumnWidth;
    sprintf(line,
			"\r|%*s%*c|%*s%*c|%*s%*c|%*s%*c|%*s%*c|%*s%*c|",
			((noOfSpaces- (int)strlen(strings[0])) >> 1) + (int)strlen(strings[0]),
			strings[0],
			((noOfSpaces- (int)strlen(strings[0])) >> 1) + ((noOfSpaces- (int)strlen(strings[0])) & 1),
			' ',
			((noOfSpaces- (int)strlen(strings[1])) >> 1) + (int)strlen(strings[1]),
			strings[1],
			((noOfSpaces- (int)strlen(strings[1])) >> 1) + ((noOfSpaces- (int)strlen(strings[1])) & 1),
			' ',
			((noOfSpaces- (int)strlen(strings[2])) >> 1) + (int)strlen(strings[2]),
			strings[2],
			((noOfSpaces- (int)strlen(strings[2])) >> 1) + ((noOfSpaces- (int)strlen(strings[2])) & 1),
			' ',
			((noOfSpaces- (int)strlen(strings[3])) >> 1) + (int)strlen(strings[3]),
			strings[3],
			((noOfSpaces- (int)strlen(strings[3])) >> 1) + ((noOfSpaces- (int)strlen(strings[3])) & 1),
			' ',
			((noOfSpaces- (int)strlen(strings[4])) >> 1) + (int)strlen(strings[4]),
			strings[4],
			((noOfSpaces- (int)strlen(strings[4])) >> 1) + ((noOfSpaces- (int)strlen(strings[5])) & 1),
			' ',
			((noOfSpaces- (int)strlen(strings[5])) >> 1) + (int)strlen(strings[5]),
			strings[5],
			((noOfSpaces- (int)strlen(strings[5])) >> 1) + ((noOfSpaces- (int)strlen(strings[5])) & 1),
			' '
			);
	return line;
}

/*----------------------------------------------------------------
 Creates the node manager, get the console ready. start threads.
 ----------------------------------------------------------------*/
int main(int argc, char **argv)
{
	
	if (!initNodesManager(&nodesManager, Filename))
		display("Cant open Config File", DisplayTypeError);
	
	int i;
	fprintf(stdout,"\033[8;%d;%dt", (int)nodesManager.used + 2, TotalWidth);
	system("clear");
	char strings[TotalColumns][ColumnWidth] = {"Philosopher", "Left Neighbor", "Right Neighbor", "Chopstick Status", "Dining Status", "isOdd"};
	
    pthread_t thread_id[nodesManager.used];
	int array[nodesManager.used];
	
	printString = (char **)calloc((nodesManager.used + 1), sizeof(char *));
	printString[0] = (char *) calloc(TotalWidth, sizeof(char));
	strncpy(printString[0], Line(strings), TotalWidth);
	printf("%s", printString[0]);
	
    for(i = 0; i < nodesManager.used; i++)
	{
		array[i] = i;
		printString[i+1] = (char *) calloc(TotalWidth, sizeof(char));
        pthread_create (&thread_id[i], NULL , getNodeStatus, &array[i]);
	}
	
//	system("tail -f Output.log");
	
    for(i = 0; i < nodesManager.used; i++)
        pthread_join(thread_id[i],NULL);
	
	system("clear");
	destroyVariablesAndExit();
	return 0;
}


