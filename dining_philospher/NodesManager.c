/********************************************************************
 
 ***** NodesManager.c *****
 
 Computer Science 6823 Distributed Systems
 
 Spring 2014
 
 Instructor:  Hai Jiang
 
 Assignment # 1
 
 Due Date:    Tuesday, Mar. 04, 2014
 
 Description:	This program provides functions for initialization and deallocation of a node manager, along with the capability of inserting nodes.
 
 *******************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

#include "DPConstants.c"

#define MaxLineSize 80
#define Delineator ':'

/*----------------------------------------------------------------
 This structure encapsulates all the varibales related to one node
 ----------------------------------------------------------------*/
typedef struct
{
	unsigned int localIPIndex;
	unsigned int leftNeighborIPIndex;
	unsigned int rightNeighborIPIndex;
	ChopStickStatus chopStickStatus;
	DiningStatus diningStatus;
	bool isOdd;
} LocalData;

LocalData emptyData = {0};
/*----------------------------------------------------------------
 This structure consists of ip address, port number for receiving
 and sending broadcast messages and port number for tcp interaction 
 with the neighbors.
 ----------------------------------------------------------------*/
typedef struct{
	char ipAddr[15];
	int broadCastPort;
	int chopstickRequestPort;
	int heartbeatPort;
} Node;

/*----------------------------------------------------------------
 This structure consists of 2d array of nodes, total size of nodelist
 and number of currently in nodelist.
 ----------------------------------------------------------------*/
typedef struct {
	Node **nodeList;
	size_t used;
	size_t size;
} NodesManager;

/*----------------------------------------------------------------
 Initialize the Node manager passed with the size required.
 ----------------------------------------------------------------*/
int initNodesManager(NodesManager *nodesManager, char *fileName);

/*----------------------------------------------------------------
 Insert the node to the nodelist of node manager.
 Reallocate memory when needed.
 Takes a str argument to calculate the values for the new node.
 ----------------------------------------------------------------*/
void insertNodesManager(NodesManager *a, char str[]);

/*----------------------------------------------------------------
 Extract the filename passed and insert nodes to list.
 ----------------------------------------------------------------*/
int extractConfigFile(NodesManager *nodesManager, char *fileName);

/*----------------------------------------------------------------
 Deallocates the node manager with all the nodes from nodelist.
 ----------------------------------------------------------------*/
void freeNodesManager(NodesManager *a);

/*----------------------------------------------------------------
----------------------------------------------------------------*/
int initNodesManager(NodesManager *nodesManager, char *fileName)
{
	int initialSize = 1;
	nodesManager->nodeList = (Node **)calloc(initialSize, sizeof(Node *));
	nodesManager->used = 0;
	nodesManager->size = initialSize;
	return extractConfigFile(nodesManager, fileName);
}

int extractConfigFile(NodesManager *nodesManager, char *fileName)
{
	int flag = 0;
	FILE *fp = fopen(fileName, "r");
	
	if (fp != NULL)
	{
		flag = 1;
		char str[MaxLineSize];
		while( fgets (str, MaxLineSize, fp) != NULL )
			insertNodesManager(nodesManager, str);
		fclose(fp);
	}
	
	return flag;
}

void insertNodesManager(NodesManager *a, char str[])
{
	if (a->used == a->size)
	{
		a->size *= 2;
		a->nodeList = (Node **)realloc(a->nodeList, a->size * sizeof(Node *));
	}
	
	a->nodeList[a->used] = (Node *)calloc(1, sizeof(Node));
	
	int i = 0, delineatorIndex = 0;
	for(i = 0; i<strlen(str); i++)
	{
		if(str[i] == Delineator)
		{
			if(delineatorIndex == 0)
				a->nodeList[a->used]->ipAddr[i] = '\0';
			delineatorIndex++;
			continue;
		}

		if (str[i] == '\n')
			break;
		
		switch(delineatorIndex)
		{
			case 0:
				a->nodeList[a->used]->ipAddr[i] = str[i];
				break;
			case 1:
				a->nodeList[a->used]->broadCastPort = a->nodeList[a->used]->broadCastPort * 10 + (str[i] - '0');
				break;
			case 2:
				a->nodeList[a->used]->chopstickRequestPort = a->nodeList[a->used]->chopstickRequestPort * 10 + (str[i] - '0');
				break;
			case 3:
				a->nodeList[a->used]->heartbeatPort = a->nodeList[a->used]->heartbeatPort * 10 + (str[i] - '0');
				break;
			default:
				break;
		}
	}
	a->used++;
}

void freeNodesManager(NodesManager *a)
{
	int i = 0 ;
	
	for (i = 0; i < a->used ; i++) {
		free(a->nodeList[i]);
	}
	free(a->nodeList);
	a->nodeList = NULL;
	a->used = a->size = 0;
}