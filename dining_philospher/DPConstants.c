/********************************************************************
 
 ***** DPConstants.c *****
 
 Computer Science 6823 Distributed Systems
 
 Spring 2014
 
 Instructor:  Hai Jiang
 
 Assignment # 1
 
 Due Date:    Tuesday, Mar. 04, 2014
 
 Description:	This program acts as library for the main programm i.e. dining.c
				and provides access to variables and delay function.
 
 *******************************************************************/


#include <time.h>
#include <stdlib.h>

#define NANO_SECOND_MULTIPLIER  1000000  // 1 millisecond = 1,000,000 Nanoseconds
#define RangeSpectrum  5000
#define MinDELAY  4
#define ERROR -1

/*----------------------------------------------------------------
 Return random delay value in the range of 4000ms-9000ms
 ----------------------------------------------------------------*/
struct timespec delay();

typedef enum
{
	ChopstickFree,
	ChopstickUsedLocal,
	ChopstickUsedRemote
} ChopStickStatus;

typedef enum
{
	SignalTypeStart,
	SignalTypeStop,
	SignalTypeCPRequest,
	SignalTypeCPReleased,
	SignalTypeCPRequestFulfilled,
	SignalTypeCPRequestDenied,
	SignalTypeStatusRequest,
	SignalTypeStatusWaiting,
	SignalTypeStatusWaiting_,
	SignalTypeStatusDining
} SignalType;

typedef enum
{
	DisplayTypeStatus,
	DisplayTypeStatusRelease,
	DisplayTypeError
} DisplayType;

typedef enum
{
	DiningStatusWaiting,
	DiningStatusWaiting_,
	DiningStatusStartedDining,
	DiningStatusThinking,
	DiningStatusFirstChopStickPicked,
	DiningStatusSecondChopStickPicked,
	DiningStatusEating,
	DiningStatusDroppingBothChopSticks,
	DiningStatusSCPNADroppingFirstChopStick,
	DiningStatusFirstChopStickNA
} DiningStatus;

typedef enum
{
	RunningModeDebug,
	RunningModeRelease
} RunningMode;

typedef struct
{
	SignalType signalType;
	unsigned int nodeIndex;
} Message;

struct timespec delay()
{
	struct timespec delayTime;
	delayTime.tv_sec = MinDELAY;
	delayTime.tv_nsec = (rand() % RangeSpectrum) * NANO_SECOND_MULTIPLIER;
	return delayTime;
}
